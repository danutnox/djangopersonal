from django.db import models


class Intrebari(models.Model):
    nume_intrebare = models.CharField(max_length=1000)
    raspuns_1 = models.TextField(max_length=5000)
    raspuns_2 = models.TextField(max_length=5000)
    raspuns_3 = models.TextField(max_length=5000)
    raspuns_4 = models.TextField(max_length=5000)
    raspuns_corect = models.TextField(max_length=5000)